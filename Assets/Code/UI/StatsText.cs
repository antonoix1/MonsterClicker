using UnityEngine.UI;
using UnityEngine;

public class StatsText : MonoBehaviour
{
    private void Start()
    {
        var saver = new JsonSaver();
        GetComponent<Text>().text = string.Format("Record: {0:f1}", saver.Load().Seconds);
    }
}
