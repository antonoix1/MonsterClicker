using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour, IBoostHandler
{
    [SerializeField] FieldToProtect _protectedField;
    [SerializeField] PlayField _field;
    [SerializeField] UIController _UI;
    [SerializeField] Sounds _sounds;
    private InputSystem _input;
    private JsonSaver _saver;
    private bool _isLost = false;

    private void Awake()
    {
        _input = new PCInput();
        _saver = new JsonSaver();
        _input.OnEnemyClicked += HandleTouch;
        _input.OnBoostClicked += HandleBoost;

        _protectedField.OnLost += HandleDefeat;
    }

    private void HandleTouch(ITouchable touched)
    {
        touched.HandleTouch();
    }

    private void HandleBoost(Boost boost)
    {
        boost.MakeBoost(this);
        _sounds.PlayBoostCollect();
    }

    private void HandleDefeat()
    {
        _isLost = true;
        _field.StopCreatingEnemies();
        _UI.ActivateRestart();
        _sounds.PlayLose();
        Save();
    }

    private void Save()
    {
        if (Time.timeSinceLevelLoad > _saver.Load().Seconds)
        {
            var data = new SaveData();
            data.Seconds = Time.timeSinceLevelLoad;
            _saver.Save(data);
        }
    }

    void Update()
    {
        if (!_isLost)
            _input.Update();   
    }

    public void MakeFreezeBoost()
    {
        if (!_isLost)
            StartCoroutine(_field.Freeze(5));
    }

    public void MakeClearBoost()
    {
        if (!_isLost)
            _protectedField.Clear();
    }
}
