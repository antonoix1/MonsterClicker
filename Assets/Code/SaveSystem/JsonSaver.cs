using System.Collections;
using System.IO;
using UnityEngine;

public class JsonSaver
{
    private readonly string _path;

    public JsonSaver()
    {
        _path = Application.persistentDataPath + "/Save.json";
    }

    public void Save(SaveData data)
    {
        var json = JsonUtility.ToJson(data);
        using (var writer = new StreamWriter(_path))
        {
            writer.WriteLine(json);
        }
    }

    public SaveData Load()
    {
        string json = "";
        using (var reader = new StreamReader(_path))
        {
            string line;
            while((line = reader.ReadLine()) != null)
            {
                json += line;
            }
        }
        if (string.IsNullOrEmpty(json))
            return new SaveData();

        return JsonUtility.FromJson<SaveData>(json);
    }
}
